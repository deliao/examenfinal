import React from "react";
import { Switch, Route } from "react-router-dom";
import ValidateRouter from "../../utilities/validateRouter";
import Login from "../Login";
import Survey from "../Survey";
import Resultado from "../Resultado";

export default function App() {
  return (
    <Switch>
      <Route path="/login" component={Login} />
      <Route path="/resultado" component={Resultado} />
      <ValidateRouter exact path="/" Component={Survey} />
    </Switch>
  );
}
