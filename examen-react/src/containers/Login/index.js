import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useAuth } from "../../utilities/useAuth";

export default props => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const auth = useAuth();
  const loginSubmit = e => {
    e.preventDefault();
    const user = {
      username: username,
      password: password
    };

    auth.login(user, () => history.push("/"));
  };
  return (
    <div>
      <h1>Login</h1>
      <form onSubmit={loginSubmit}>
        <div>
          <label htmlFor="username">usuario </label>
          <input
            id="username"
            type="text"
            name="username"
            onChange={e => setUsername(e.target.value)}
            value={username}
          />
        </div>
        <div>
          <label htmlFor="password">password </label>
          <input
            id="password"
            type="password"
            name="password"
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
        </div>
        <input type="submit" value="Iniciar Sesion " />
      </form>
    </div>
  );
};
