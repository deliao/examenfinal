import React from "react";

export default () => {
  return (
    <div>
      <h3>Resultado</h3>
      <label>{localStorage.getItem("resultado")} correctas</label>
    </div>
  );
};
