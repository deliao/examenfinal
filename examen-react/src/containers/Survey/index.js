import React from "react";
import { WrapperContainer } from "../../component";
import { options } from "../../utilities/surveyOptions";

export default () => {
  return <WrapperContainer arrayForm={options} />;
};
