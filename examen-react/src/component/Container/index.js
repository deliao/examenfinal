import React from "react";
import styled from "styled-components";

const Main = styled.div`
  background: #9be7ff;
  border: 1px solid;
  margin: 5px;
`;

const Container = ({ titulo, children }) => {
  return (
    <Main>
      <h3>{titulo}</h3>
      <div>{children}</div>
    </Main>
  );
};

export default Container;
