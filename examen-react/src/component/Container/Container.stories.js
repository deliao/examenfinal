import React from "react";
import Container from ".";

export default {
  title: "Container",
  component: Container,
  parameters: {
    info: {
      inline: true
    }
  }
};

export const defaultContainer = () => (
  <Container titulo="Titulo">
    <p>Contenido</p>
  </Container>
);
