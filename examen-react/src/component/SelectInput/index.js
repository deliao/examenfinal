import React from "react";
import { Formik, Form, Field } from "formik";
import Select from "react-select";
import { useAuth } from "../../utilities/useAuth";
import { useHistory } from "react-router-dom";

const SelectInput = ({ opciones, callback, indice, length }) => {
  const data = opciones.opciones.map(opt => ({
    label: opt.nombreOpcion,
    value: opt.esVerdadera
  }));
  console.log("data " + data);
  //const finisCtx = useContext(FinishContext);
  const finisCtx = useAuth();
  const history = useHistory();

  const SelectF = props => {
    console.log("props: ", props.options);

    return (
      <Select
        options={props.options}
        {...props.field}
        onChange={option => {
          props.form.setFieldValue(props.field.name, option);
          option.value &&
            localStorage.setItem(
              "resultado",
              parseInt(localStorage.getItem("resultado")) + parseInt(1)
            );
        }}
      />
    );
  };

  return (
    <Formik
      initialValues={{ data, selectComp: "", opciones }}
      validate={field => {
        !field.selectComp && alert("seleccione una opción");
      }}
      onSubmit={() => {
        if (parseInt(indice) + 1 !== length) callback(parseInt(indice) + 1);
        else {
          finisCtx.onFinish(true);
          history.push("/resultado");
        }
      }}
    >
      {({ values, handleSubmit }) => (
        <Form onSubmit={handleSubmit}>
          <Field name="selectComp" component={SelectF} options={values.data} />
          {parseInt(indice) + 1 === length ? (
            <button>Finalizar</button>
          ) : (
            <button>Siguiente</button>
          )}
        </Form>
      )}
    </Formik>
  );
};

export default SelectInput;
