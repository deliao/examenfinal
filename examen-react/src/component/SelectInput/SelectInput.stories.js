import React from "react";
import SelectInput from ".";
import { form1 } from "../../utilities/surveyOptions";

export default {
  title: "Select Input",
  component: SelectInput,
  parameters: {
    info: {
      inline: true
    }
  }
};

export const defaultSelect = () => <SelectInput contenido={form1} />;
