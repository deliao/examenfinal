import React, { useState, createContext } from "react";
import Container from "../Container";
import SelectInput from "../SelectInput";

export const FinishContext = createContext({});
const res = localStorage.setItem("resultado", 0);
export default ({ arrayForm }) => {
  const [indice, setIndice] = useState(0);

  const callback = nextVal => {
    setIndice(nextVal);
  };

  return (
    <Container key={indice} titulo={arrayForm[indice].pregunta}>
      <SelectInput
        id={indice}
        opciones={arrayForm[indice]}
        callback={callback}
        indice={indice}
        length={arrayForm.length}
      />
    </Container>
  );
};
