export const options = [
  {
    pregunta: "la suma de 2 mas 2 es:",
    opciones: [
      {
        nombreOpcion: "4",
        esVerdadera: true
      },
      {
        nombreOpcion: "3",
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: "la multiplicacion de 2 *2:",
    opciones: [
      {
        nombreOpcion: "4",
        esVerdadera: true
      },
      {
        nombreOpcion: "5",
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: "la division de 2 / 2 es:",
    opciones: [
      {
        nombreOpcion: "1",
        esVerdadera: true
      },
      {
        nombreOpcion: "4",
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: "la resta de 2 - 2 es:",
    opciones: [
      {
        nombreOpcion: "0",
        esVerdadera: true
      },
      {
        nombreOpcion: "7",
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: "2*9 es:",
    opciones: [
      {
        nombreOpcion: "4",
        esVerdadera: false
      },
      {
        nombreOpcion: "18",
        esVerdadera: true
      }
    ]
  }
];
