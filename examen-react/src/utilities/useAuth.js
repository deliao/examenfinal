import React, { useContext, useState, createContext } from "react";
import validarSesion from "./validarSesion";
import axios from "axios";

export const Context = createContext({});

export const AuthProvider = props => {
  const sesion = validarSesion();
  const [isAuthenticated, setIsAuthenticated] = useState(sesion);

  const [isFinish, setIsFinish] = useState(false);

  const onFinish = val => {
    setIsFinish(val);
  };

  const login = async (user, callback) => {
    const res = await axios.post(
      "https://login-test-dga.herokuapp.com/login",
      user
    );

    if (res.data.response) {
      localStorage.setItem("username", user.username);
      localStorage.setItem("password", user.password);
      setIsAuthenticated(true);

      callback();
    }
  };
  return (
    <Context.Provider value={{ isAuthenticated, login, onFinish }}>
      {props.children}
    </Context.Provider>
  );
};

export const useAuth = () => {
  return useContext(Context);
};
