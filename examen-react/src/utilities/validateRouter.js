import React from "react";
import { Route, Redirect } from "react-router-dom";
import { useAuth } from "./useAuth";

export default ({ path, Component }) => {
  const auth = useAuth();
  return (
    <Route
      path={path}
      component={() =>
        auth.isAuthenticated ? <Component /> : <Redirect to="/login" />
      }
    />
  );
};
